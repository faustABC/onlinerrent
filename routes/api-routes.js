const router = require('express').Router();
const multer = require('multer');
const imagesController = require('./images');
const zipController = require('./zip');

const upload = multer();

router.route('/image')
  .post(upload.single('file'), imagesController.load)
  .get(imagesController.sendImage)
  .put(imagesController.changeData);

router.route('/zip')
  .get(zipController.sendZip);

module.exports = router;

const { readFileSync } = require('fs');
const parse = require('../src/utils/zip/parse');
const unZip = require('../src/utils/zip/unzip');
const { getSrc } = require('../models/images');
const exportArtboard = require('../src/utils/sketchtool');

exports.load = async (_req, _res) => {
  parse(_req.file);
  _res.sendStatus(200);
};

exports.sendImage = async (_req, _res) => {
  const src = await getSrc();
  _res.json(src);
};

exports.changeData = async (_req, _res) => {
  const { unzip } = await getSrc();
  const { text, color } = _req.body.data;

  await unZip(unzip, text, color);
  setTimeout(async () => {
    await exportArtboard();

    const base64Image = await readFileSync(
      `${process.env.ZIP_PATH}/iPhone SE.png`,
      'base64',
    );

    _res.json({ src: `data:image/jpeg;base64, ${base64Image}` });
  }, 2000);
};

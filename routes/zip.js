const path = require('path');

exports.sendZip = async (_req, _res) => {
  const filePath = path.join(process.env.ZIP_PATH, '/zip.sketch');

  _res.sendFile(filePath);
};

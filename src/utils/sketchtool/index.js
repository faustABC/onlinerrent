const sketchtool = require('sketchtool-cli');

module.exports = () => {
  sketchtool.run(`export artboards ${process.env.ZIP_PATH}/zip.sketch --output=${process.env.ZIP_PATH}`);
};

/* eslint-disable no-loop-func */
/* eslint-disable no-restricted-syntax */
const JSZip = require('jszip');
const { imagesModel } = require('../../../models/images');

const parse = async (file) => {
  await JSZip.loadAsync(file.buffer).then(async (zip) => {
    for await (const [relativePath, zipEntry] of Object.entries(zip.files)) {
      if (relativePath === 'previews/preview.png') {
        zipEntry.async('base64').then(async (content) => {
          await imagesModel.findOneAndUpdate({}, { src: `data:image/png;base64,${content}`, unzip: file });
        });
      } else if (relativePath === 'pages/9B2D6A75-D2C0-40F3-88AB-AE4BD625E520.json') {
        zipEntry.async('string').then(async (content) => {
          const imageData = JSON.parse(content);
          await imagesModel.findOneAndUpdate({}, { imageData });
        });
      }
    }
  });
};

module.exports = parse;

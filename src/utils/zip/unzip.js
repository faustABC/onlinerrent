/* eslint-disable no-multi-assign */
/* eslint-disable no-loop-func */
/* eslint-disable no-restricted-syntax */
const mkdirp = require('mkdirp');
const JSZip = require('jszip');
const { writeFileSync } = require('fs');
const hexRgb = require('hex-rgb');

const writeFile = async (path, content) => {
  const mkdirPath = path.split('/');
  mkdirPath.pop();
  await mkdirp(mkdirPath.join('/'));
  setTimeout(() => writeFileSync(path, content), 100);
};

const generateZip = () => {
  const zip = new JSZip();

  return {
    addFile: async (path, content) => {
      await zip.file(path, content);
    },
    saveFile: async () => {
      const blob = await zip.generateAsync({ type: 'nodebuffer' });
      writeFileSync(`${process.env.ZIP_PATH}/zip.sketch`, blob);
    },
  };
};

const unzip = async (file, text, color) => {
  const { addFile, saveFile } = generateZip();

  await JSZip.loadAsync(file.buffer.buffer).then(async (zip) => {
    for await (const [relativePath, zipEntry] of Object.entries(zip.files)) {
      const destination = relativePath;
      if (relativePath === 'pages/9B2D6A75-D2C0-40F3-88AB-AE4BD625E520.json') {
        zipEntry.async('string').then(async (content) => {
          const newContent = JSON.parse(content);
          newContent.layers[0].layers[0].layers[0].attributedString.string = text;
          const { MSAttributedStringColorAttribute } = newContent.layers[0].layers[0].layers[0].attributedString.attributes[0].attributes;
          const newColor = { ...MSAttributedStringColorAttribute, ...hexRgb(color,) };
          newColor.red = newColor.red / 225;
          newColor.green = newColor.green / 225;
          newColor.blue = newColor.blue / 225;
          console.log(newColor);
          newContent.layers[0].layers[0].layers[0].attributedString.attributes[0].attributes.MSAttributedStringColorAttribute = newColor;
          await addFile(destination, JSON.stringify(newContent));
        });
      } else {
        zipEntry.async('nodebuffer').then(async (content) => {
          await addFile(destination, content);
        });
      }
    }
  });

  setTimeout(() => saveFile(), 1000);
};

module.exports = unzip;

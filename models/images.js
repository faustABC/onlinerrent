const mongoose = require('mongoose');

const { Schema } = mongoose;

const imagesSchema = new Schema([{
  src: String,
  unzip: Schema.Types.Mixed,
  imageData: Schema.Types.Mixed,
}]);

exports.imagesModel = mongoose.model('images', imagesSchema);

exports.getSrc = () => this.imagesModel.findOne({});

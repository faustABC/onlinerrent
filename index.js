require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();

const apiRoutes = require('./routes/api-routes');

app.use(bodyParser.urlencoded({
  extended: true,
  limit: '50mb',
}));

app.use(bodyParser.json({
  extended: true,
  limit: '50mb',
}));

mongoose.connect(process.env.MONGO_CONNECTION_STRING, {
  useNewUrlParser: true,
  useFindAndModify: false,
});

mongoose.Promise = global.Promise;

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const port = process.env.PORT || 3000;

app.use((_req, _res, _next) => {
  _res.header('Access-Control-Allow-Origin', '*');
  _res.header('Access-Control-Allow-Methods', 'POST, PUT, GET, OPTIONS');
  _res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  _next();
});

app.get('/', (req, res) => res.send('Hello World with Express'));
app.use('/api', apiRoutes);

app.listen(port, () => {
  console.log(`Running Rents backend on port ${port}`);
});
